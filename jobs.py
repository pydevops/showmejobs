#!/usr/bin/env python

''' works for python 2.7.x
'''
from __future__ import print_function
from bs4 import BeautifulSoup
#import urllib2
import requests
from contextlib import closing

url = 'http://www.python.org/community/jobs/'
jobs = 'jobs.html'

def soup_jobs(job_url,file_out):
	'''
	xform the job html to file_out with BeautifulSoup4
	'''
	#with closing(urllib2.urlopen(job_url)) as stream, open(file_out, 'wt') as myout:
	with closing(requests.get(url)) as stream, open(file_out, 'wt') as myout:
	        soup = BeautifulSoup(stream.text)

	        print("<html><body>", file=myout)
	        sections = soup.findAll('div', {'class': 'section'})
	        for section in sections:
	            #find the job which has allowed the telecommuting
	            if section.findAll(lambda tag: tag.findAll('li', text='Telecommuting OK')):
	                print(section, file=myout)
	        print("</body></html>", file=myout)

if __name__ == '__main__':
	soup_jobs(url, jobs)
